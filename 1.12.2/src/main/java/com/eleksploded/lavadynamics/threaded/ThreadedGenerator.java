package com.eleksploded.lavadynamics.threaded;

import java.util.Deque;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang3.tuple.Pair;

import com.eleksploded.lavadynamics.LavaConfig;
import com.eleksploded.lavadynamics.LavaDynamics;
import com.eleksploded.lavadynamics.Volcano;
import com.eleksploded.lavadynamics.storage.CheckedCap;
import com.eleksploded.lavadynamics.storage.VolcanoRangeChecker;
import com.google.common.util.concurrent.Callables;

import net.minecraft.server.MinecraftServer;
import net.minecraft.world.chunk.Chunk;

public class ThreadedGenerator extends Thread {
	public void debug(String message) { if(LavaConfig.general.genVolcanoDebug) LavaDynamics.Logger.info(message); }
	private static ThreadedGenerator instance = null;

	MinecraftServer server;
	private Deque<Chunk> chunkQueue = new ConcurrentLinkedDeque<>();
	private Deque<Pair<Chunk, Future<Boolean>>> completeQueue = new ConcurrentLinkedDeque<>();
	private Completer completer;
	private Random rand;

	private ExecutorService executorService;
	ExecutorService pool;

	private ThreadedGenerator(MinecraftServer server) {
		debug("Starting Theaded Generator");
		instance = this;
		this.server = server;
		this.setDaemon(true);
		this.setName("LD - Threaded Generator");
		rand = new Random();
		pool = Executors.newFixedThreadPool(LavaConfig.threadedOptions.threadPoolCount);

		completer = new Completer();
		completer.start();
		this.start();
	}

	public static ThreadedGenerator getOrCreate(MinecraftServer server) {
		if(instance == null) {
			if(server == null) {
				throw new RuntimeException("Attempted to create threaded generator without server reference");
			}
			new ThreadedGenerator(server);
		}
		return instance;
	}

	public void queue(Chunk c) {
		chunkQueue.add(c);
	}

	public void end() {
		instance = null;
	}

	public void run() {
		debug("Threaded Generator Beginning");
		while(server.isServerRunning() && instance != null) {
			//debug("Threaded Generator Run");
			if(!chunkQueue.isEmpty()) {
				Chunk c = chunkQueue.poll();
				debug("Working on " + c.getPos());
				if(LavaConfig.volcano.disaster) {
					completeQueue.add(Pair.of(c, pool.submit(Callables.returning(false))));
				} else if(isChunkGeneratable(c)) {
					debug("Beginning range check on " + c.getPos());
					Future<Boolean> rangeCheck = VolcanoRangeChecker.getForDim(c.getWorld().provider.getDimension()).isVolcanoInRangeThreaded(server, c, LavaConfig.volcano.distance, pool);
					completeQueue.add(Pair.of(c, rangeCheck));
				}
			}
		}
		instance = null;
		pool.shutdownNow();
		debug("Ended Threaded Generator");
	}

	private boolean isChunkGeneratable(Chunk c) {

		if(c.getCapability(CheckedCap.checkedCap, null).isChecked()) {
			return false;
		}

		c.getCapability(CheckedCap.checkedCap, null).check();
		debug("Checking " + c.getPos());

		if(LavaConfig.volcano.protectChunks && !c.getTileEntityMap().isEmpty()) {
			return false;
		}

		if(!LavaConfig.volcano.spawnChunks && c.getWorld().isSpawnChunk(c.x, c.z)) {
			return false;
		}

		return true;
	}
	
	public static boolean isActive() {
		return instance != null;
	}

	private class Completer extends Thread {

		private Completer() {
			debug("Starting Theaded Completer");
			this.setDaemon(true);
			this.setName("LD - Threaded Completer");
		}

		public void run() {
			while(instance != null) {
				if(!completeQueue.isEmpty()) {
					if(completeQueue.peek().getRight().isDone()) {
						try {
							Pair<Chunk, Future<Boolean>> pair = completeQueue.poll();

							if(!pair.getRight().get()) {
								int num = rand.nextInt(100) + 1;
								if(num <= LavaConfig.volcano.volcanoChance) {
									Chunk chunk = pair.getLeft();
									chunk.getCapability(CheckedCap.checkedCap, null).setVolcano(-1);
									
									if(LavaConfig.threadedOptions.asyncBuilding) {
										debug("Queued Volcano at " + chunk.getPos());
										AsyncBlockPlacer.queueVolcano(chunk, chunk.getWorld());
									} else {
										server.addScheduledTask(() -> Volcano.genVolcano(chunk, chunk.getWorld()));
									}
								}
							}
						} catch (InterruptedException | ExecutionException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}
