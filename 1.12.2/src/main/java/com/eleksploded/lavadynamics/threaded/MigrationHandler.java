package com.eleksploded.lavadynamics.threaded;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;

import com.eleksploded.lavadynamics.storage.CheckedCap;

import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class MigrationHandler {
	
	private static final String checkedFile = "LD_CheckedStorage";
	private static final String volcanoFile = "LD_VolcanoStorage";
	
	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public static void chunkLoad(ChunkEvent.Load e) throws IOException {
		Chunk c = e.getChunk();
		int dim = c.getWorld().provider.getDimension();
		if(getFile(dim, checkedFile) != null) {
			if(Files.lines(getFile(dim, checkedFile).toPath()).anyMatch((l) -> l.equals(c.x + "|" + c.z))) {
				c.getCapability(CheckedCap.checkedCap, null).check();
			}
		}
		
		if(getFile(dim, volcanoFile) != null) {
			Optional<String> str = Files.lines(getFile(dim, volcanoFile).toPath()).filter((l) -> l.equals(c.x + "|" + c.z)).findFirst();
			if(str.isPresent()) {
				String s[] = str.get().split("\\|");
				try {
					int top = Integer.valueOf(s[2]);
					c.getCapability(CheckedCap.checkedCap, null).check();
					c.getCapability(CheckedCap.checkedCap, null).setVolcano(top);
				} catch(NumberFormatException ex) {
				}
			}
		}
	}
	
	private static File getFile(int dimID, String fileName) {
		String tmp = DimensionManager.getCurrentSaveRootDirectory() + "/";

		if(DimensionManager.getProvider(dimID).getSaveFolder() != null) {
			tmp = tmp + DimensionManager.getProvider(dimID).getSaveFolder() + "/" + fileName;
		} else {
			tmp = tmp + fileName;
		}
		File file = new File(tmp);
		
		if(!file.exists()) {
			return null;
		}
		return file;
	}
}
