package com.eleksploded.lavadynamics.threaded.worldaction;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.world.World;

public class ExplosionWorldAction implements IWorldAction {
	@Nullable
	private Entity sourceEntity;
	private double x;
	private double y;
	private double z;
	private float strength;
	private boolean fire;
	private boolean damage;
	
	public ExplosionWorldAction(@Nullable Entity sourceEntity, double x, double y, double z, float strength, boolean fire,
			boolean damage) {
		super();
		this.sourceEntity = sourceEntity;
		this.x = x;
		this.y = y;
		this.z = z;
		this.strength = strength;
		this.fire = fire;
		this.damage = damage;
	}

	@Override
	public void takeAction(World world) {
		world.newExplosion(sourceEntity, x, y, z, strength, fire, damage);
	}

}
